import React from 'react';
import { Box } from '..';
import { ImageBackground, StyleSheet } from 'react-native';
import { ColorType } from '../types';

export const BackgroundImage: React.FC<BackgroundImageType> = (props) => {
  const {
    source,
    overflow,
    resizeMode,
    height,
    width,
    children,
    role,
    opacity,
    ratio = 1,
  } = props;
  const styles = StyleSheet.create({
    image: {
      flex: source && 1,
      justifyContent: 'center',
      opacity,
      aspectRatio: ratio || 1,
      alignItems: 'center'
    },
  });
  return (
    <Box
      position={'relative'}
      overflow={overflow}
      width={width}
      height={height}
      role={role}
    >
      <ImageBackground
        source={source}
        style={[styles.image]}
        resizeMode={resizeMode || 'stretch'}
      >
        {children}
      </ImageBackground>
    </Box>
  );
};

type BackgroundImageType = {
  source: any;
  overflow?: 'hidden' | 'scroll' | 'visible';
  resizeMode?: 'cover' | 'contain' | 'stretch' | 'repeat' | 'center';
  role?: ColorType;
  opacity?: number;
  width?: string | number;
  height?: string | number;
  children?: JSX.Element | JSX.Element[] | string;
  ratio?: number;
};
