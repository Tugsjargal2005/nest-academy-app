import React from 'react';
import { Avatar, Border, Box, Queue, Shadow, Spacing, Stack, Text } from '.';

export const Comment: React.FC<Comments> = ({
  name,
  source,
  title,
  description,
  width,
  height,
}) => {
  return (
    <Spacing mr={2}>
      <Border lineWidth={1} role="primary200" radius={4}>
        <Shadow radius={2} role="gray">
          <Box width={width || 311} height={height || 282} role="white">
            <Spacing pv={3} ph={4}>
              <Queue justifyContent="flex-start">
                <Spacing mr={3}>
                  <Avatar size="M" source={source} />
                </Spacing>
                <Stack justifyContent="center">
                  <Text type="subheading" opacity={0.5} role="black">
                    {title}
                  </Text>
                  <Text type="body" role="black" bold>
                    {name}
                  </Text>
                </Stack>
              </Queue>
            </Spacing>
            <Border topWidth={1} role="primary200">
              <Spacing mv={5} mh={6}>
                <Box>
                  <Text role="black" type="body">
                    {description}
                  </Text>
                </Box>
              </Spacing>
            </Border>
          </Box>
        </Shadow>
      </Border>
    </Spacing>
  );
};

interface Comments {
  name?: string;
  source?: string;
  title?: string;
  description?: string;
  width?: number | string;
  height?: number | string;
}
