import React from 'react'
import { Image, TouchableOpacity } from 'react-native'
import { Box, Text, Border, Spacing } from '../components'
import { ResumeIcon } from './icons'
import { useNavigation } from '@react-navigation/native';
import { NavigationRoutes } from '../navigation/navigation-params';

const LessonVideo = require('../assets/images/videoLessonImage.png')

interface Items {
    onPress?: Function;
    previewImage?: any;
    processBar?: any
}

export const LastSeenVideo: React.FC<Items> = ({ onPress, previewImage, processBar }) => {
    const navigation = useNavigation();

    return (
        <Box width={'100%'}>
            <Text bold fontFamily="Montserrat" type={'body'}  >
                СҮҮЛД ҮЗСЭН ХИЧЭЭЛ
            </Text>
            <Spacing mt={3} />
            <TouchableOpacity activeOpacity={0.2} onPress={() => onPress ? onPress() : navigation.navigate(NavigationRoutes.HopScreen) } >
                <Box justifyContent={'center'} alignItems={'center'}>
                    <Image resizeMode={'stretch'} style={{ width: '100%' }} source={previewImage ? previewImage : LessonVideo} />
                    <Box position="absolute">
                        <ResumeIcon />
                    </Box>
                </Box>
                <Box>
                    <Box width={'100%'} position={'absolute'}>
                        <Border role={'gray'} lineWidth={3} radius={3} />
                    </Box>
                    <Box width={processBar ? processBar : '30%'} position={'absolute'}>
                        <Border role={'accentNest'} lineWidth={3} radius={3} />
                    </Box>
                </Box>
            </TouchableOpacity>
        </Box>
    )
}