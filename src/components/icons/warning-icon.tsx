import React from 'react';
import Svg, { Path } from 'react-native-svg';
import { useTheme } from '../theme-provider';
import { IconType } from '../types';

export const WarningIcon: React.FC<IconType> = ({
  height = 15,
  width = 15,
  role = 'caution500',
}) => {
  const { colors } = useTheme();
  return (
    <Svg width={width} height={height} viewBox="0 0 18 15" fill="none">
      <Path
        d="M13.125 0.375L17.25 7.5L13.125 14.625H4.875L0.75 7.5L4.875 0.375H13.125ZM12.2603 1.875H5.73975L2.48325 7.5L5.73975 13.125H12.2603L15.5167 7.5L12.2603 1.875ZM8.25 9.75H9.75V11.25H8.25V9.75ZM8.25 3.75H9.75V8.25H8.25V3.75Z"
        fill={colors[role]}
      />
    </Svg>
  );
};
