import React from 'react';
import Svg, { Path } from 'react-native-svg';
import { useTheme } from '../theme-provider';
import { IconType } from '../types';

export const CircleIcon: React.FC<IconType> = ({ height = 28, width = 28 }) => {
  const { colors } = useTheme();
  return (
    <Svg width={width} height={height} viewBox="0 0 28 28" fill="none">
      <Path
        d="M14 27.334C6.634 27.334.665 21.364.665 14S6.636.667 13.999.667c7.364 0 13.334 5.97 13.334 13.333 0 7.364-5.97 13.334-13.334 13.334zm0-2.667a10.667 10.667 0 100-21.333 10.667 10.667 0 000 21.333z"
        fill="#E8E8E8"
      />
    </Svg>
  );
};
