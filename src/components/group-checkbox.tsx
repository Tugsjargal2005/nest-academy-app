import React, { createContext, useContext, useEffect, useState } from 'react';
import { Border, Button, Shadow } from './core';
import { Box, Queue, Spacing, Stack } from './layout';
import _ from 'lodash';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { BannerSuccessIcon } from './icons/bannerIcons';
import { SuccessIconBold } from './icons/success-icon-bold';

type GroupCheckboxType = {
  horizontal?: boolean;
  children: any;
  defaultValue?: number;
  size?: number;
};

type GroupCheckboxItemType = {
  checkbox?: boolean;
  children?: any;
  index: number;
  onPress?: any;
  onUnPress?: any;
  size?: any;
};

export const CheckBoxContext = createContext({
  checked: null,
  setChecked: (number: any) => {},
  horizontal: null,
});

export const GroupCheckBox: React.FC<GroupCheckboxType> = ({
  horizontal,
  children,
  defaultValue,
  size = 4,
}) => {
  const [checked, setChecked]: any = useState(
    defaultValue ? defaultValue : null
  );

  return (
    <CheckBoxContext.Provider value={{ checked, setChecked, horizontal }}>
      <Box flexDirection={horizontal ? 'column' : 'row'} width="100%">
        {horizontal ? (
          <Stack size={size} width={'100%'} justifyContent={'space-between'}>
            {children}
          </Stack>
        ) : (
          <Queue width={'100%'} size={size} justifyContent={"space-around"}>
            {children}
          </Queue>
        )}
      </Box>
    </CheckBoxContext.Provider>
  );
};

export const CheckBoxItem: React.FC<GroupCheckboxItemType> = ({
  children,
  checkbox,
  index,
  onPress,
  onUnPress,
  size = [20, 16, 20],
}) => {
  const {
    setChecked: set,
    checked: check,
    horizontal,
  } = useContext(CheckBoxContext);
  const [checked, setChecked] = useState(false);

  useEffect(() => {
    check === index ? setChecked(true) : setChecked(false);
  }, [check]);

  const onPressed = () => {
    if (check == index) {
      set(null);
      onUnPress();
    } else {
      set(index);
      onPress();
    }
  };

  return (
    <>
      {!horizontal ? (
        <Box height={123}>
          <TouchableOpacity onPress={() => onPressed()}>
            <Border
              lineWidth={1}
              role={checked ? 'success400' : 'lightgray'}
              radius={8}
            >
              <>
                {checkbox && (
                  <Box position="absolute" top={10} right={10}>
                    <Border
                      lineWidth={checked ? 0 : 2}
                      role={'lightgray'}
                      radius={20}
                    >
                      <Box width={checked ? size[0] : size[1]} height={checked ? size[0] : size[1]}>
                        {checked && <SuccessIconBold height={size[0]} width={size[0]} />}
                      </Box>
                    </Border>
                  </Box>
                )}
                <Box alignItems="center">{children}</Box>
              </>
            </Border>
          </TouchableOpacity>
        </Box>
      ) : (
        <TouchableOpacity onPress={() => onPressed()}>
          <Shadow role={'gray'} radius={1} opacity={0.15} h={0} w={0}>
            <Border
              lineWidth={1}
              role={checked ? 'success400' : 'lightgray'}
              radius={8}
            >
              <Spacing pr={checkbox ? 4.5 : 0}>
                <Box
                  flexDirection={'row'}
                  justifyContent="space-between"
                  alignItems="center"
                >
                  {children}
                  {checkbox && (
                    <Border
                      lineWidth={checked ? 0 : 2}
                      role={'lightgray'}
                      radius={20}
                    >
                      <Box width={checked ? 26 : 22} height={checked ? 26 : 22}>
                        {checked && <SuccessIconBold />}
                      </Box>
                    </Border>
                  )}
                </Box>
              </Spacing>
            </Border>
          </Shadow>
        </TouchableOpacity>
      )}
    </>
  );
};
