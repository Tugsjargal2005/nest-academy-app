export enum NavigationRoutes {
  MainRoot = 'MainRoot',
  Home = 'Home',
  SignUp = 'SignUp',
  LogIn = 'LogIn',
  Course = 'Course',
  Profiles = 'Profiles',
  Profile = 'Profile',
  ProfileNotLoggedIn = 'ProfileNotLoggedIn',
  Admission = 'Admission',
  PersonalInformation = 'PersonalInformation',
  Test = 'Test',
  Splash = 'Splash',
  FaqScreen = 'FaqScreen',
  HopScreen = 'HopScreen',
  LeapScreen = 'LeapScreen',
  ExamWarning1 = 'ExamWarning1',
  ExamWarning2 = 'ExamWarning2',
  ExamWarning3 = 'ExamWarning3',
  ExamWarning4 = 'ExamWarning4',
  ExamScreen = 'ExamScreen',
  ExamResultScreen = 'ExamResultScreen',
  Success = 'Success',
  HomeScreen = 'HomeScreen',
  InternetFailedScreen = 'InternetFailedScreen',
  MicroCoursePopupScreen = 'MicroCoursePopupScreen',
  RegistrationSuccessScreen = 'RegistrationSuccessScreen',
  Registration = 'Registration',
  DemoDetails = 'DemoDetails',
  WelcomeScreen = 'WelcomeScreen',
  News = 'News',
  FreeCourseScreen = 'FreeCourseScreen',
  NotificationScreen = 'NotificationScreen',
  ScheduleMeetingCalendar = 'ScheduleMeetingCalendar',
  ScheduleMeetingTime = 'ScheduleMeetingTime',
  NoInternetConnectionScreen = ' NoInternetConnectionScreen',
  SomethingWentWrongScreen = 'SomethingWentWrongScreen',
  EnableNotificationScreen = 'EnableNotificationScreen',
  ExamStack = 'ExamStack',
  BookMeeting = 'BookMeeting',
  MainCourseRegistration = 'MainCourseRegistration',
  CurriculmScreen = 'CurriculmScreen',
  SuccessScreen = 'SuccessScreen'
}
export interface NavigationPayload<T> {
  props: T;
}
export type NavigatorParamList = {
  [NavigationRoutes.Splash]: NavigationPayload<any>;
  [NavigationRoutes.SignUp]: NavigationPayload<any>;
  [NavigationRoutes.LogIn]: NavigationPayload<any>;
  [NavigationRoutes.MainRoot]: NavigationPayload<any>;
  [NavigationRoutes.Home]: NavigationPayload<any>;
  [NavigationRoutes.Course]: NavigationPayload<any>;
  [NavigationRoutes.Profile]: NavigationPayload<any>;
  [NavigationRoutes.FaqScreen]: NavigationPayload<any>;
  [NavigationRoutes.ProfileNotLoggedIn]: NavigationPayload<any>;
  [NavigationRoutes.Admission]: NavigationPayload<any>;
  [NavigationRoutes.PersonalInformation]: NavigationPayload<any>;
  [NavigationRoutes.Test]: NavigationPayload<any>;
  [NavigationRoutes.NotificationScreen]: NavigationPayload<any>;
  [NavigationRoutes.CurriculmScreen]: NavigationPayload<any>;
  [NavigationRoutes.Splash]: NavigationPayload<any>;
  [NavigationRoutes.HopScreen]: NavigationPayload<any>;
  [NavigationRoutes.LeapScreen]: NavigationPayload<any>;
  [NavigationRoutes.ExamStack]: NavigationPayload<any>;
  [NavigationRoutes.BookMeeting]: NavigationPayload<any>;
  [NavigationRoutes.MainCourseRegistration]: NavigationPayload<any>;
  [NavigationRoutes.ExamWarning1]: NavigationPayload<any>;
  [NavigationRoutes.ExamWarning2]: NavigationPayload<any>;
  [NavigationRoutes.ExamWarning3]: NavigationPayload<any>;
  [NavigationRoutes.ExamWarning4]: NavigationPayload<any>;
  [NavigationRoutes.FreeCourseScreen]: NavigationPayload<any>;
  [NavigationRoutes.News]: NavigationPayload<any>;
  [NavigationRoutes.ExamScreen]: NavigationPayload<any>;
  [NavigationRoutes.ExamResultScreen]: NavigationPayload<any>;
  [NavigationRoutes.Success]: NavigationPayload<{ successMessage: string }>;
  [NavigationRoutes.HomeScreen]: NavigationPayload<{ HomeScreen: string }>;
  [NavigationRoutes.InternetFailedScreen]: NavigationPayload<{
    description: string;
    title: string;
    icon: any;
    onClick: () => void;
  }>;
  [NavigationRoutes.MicroCoursePopupScreen]: NavigationPayload<{
    description: string;
    title: string;
    icon: any;
    onClick: () => void;
  }>;
  [NavigationRoutes.RegistrationSuccessScreen]: NavigationPayload<{
    admissionId: string;
  }>;
  [NavigationRoutes.Registration]: NavigationPayload<any>;
  [NavigationRoutes.DemoDetails]: NavigationPayload<any>;
  [NavigationRoutes.WelcomeScreen]: NavigationPayload<any>;
  [NavigationRoutes.Profiles]: NavigationPayload<any>;
  [NavigationRoutes.ScheduleMeetingCalendar]: NavigationPayload<any>;
  [NavigationRoutes.ScheduleMeetingTime]: NavigationPayload<any>;
  [NavigationRoutes.NoInternetConnectionScreen]: NavigationPayload<{
    description: string;
    title: string;
    icon: any;
    onClick: () => void;
  }>;
  [NavigationRoutes.SomethingWentWrongScreen]: NavigationPayload<{
    description: string;
    title: string;
    icon: any;
    onClick: () => void;
  }>;
  [NavigationRoutes.EnableNotificationScreen]: NavigationPayload<{
    description: string;
    title: string;
    icon: any;
    onClick: () => void;
  }>;
  [NavigationRoutes.SuccessScreen]: NavigationPayload<{
    description: string;
    title: string;
    icon: any;
    onClick: () => void;
  }>;
};
