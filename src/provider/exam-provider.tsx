import React, { createContext, useState, useEffect, useContext } from 'react';
import { useQuery } from '@apollo/client';
import { REQUEST_EXAM_SCREEN } from '../graphql/queries';
import _ from 'lodash';
import { NavigationRoutes } from '../navigation/navigation-params'
import { useNavigation } from '@react-navigation/core';
import { AuthContext } from './auth';
import { useDocument } from '../hooks';

type QuizType = {
  id: string;
  question: string;
  questionPicture: string;
  answerPicture: string;
  correctAnswers: string;
  point: number;
  answers: any;
  type: string;
  score: number;
  userAnswer: null | string;
}

export const ExamContext = createContext<any>({
  quizes: [],
  setQuizes: () => { },
  updateAnswer: () => {},
  finishExam: () => {},
});

export const ExamProvider = ({ children }: any) => {
  const { data, loading } = useQuery(REQUEST_EXAM_SCREEN);
  const [quizes, setQuizes] = useState<Array<QuizType>>([]);
  const navigation = useNavigation();
  const { user } = useContext(AuthContext);
  const { updateRecord } = useDocument(`users/${user?.uid}`)

  useEffect(() => {
    if (!loading && data) {
      const iqQuizes = data?.exam?.iqCollection.items;
      const designQuizes = data?.exam?.designCollection.items;
      const logicQuizes = data?.exam?.logicCollection.items;
      const mathQuizes = data?.exam?.mathCollection.items;

      const quizItems: any = _.concat(
        _.sampleSize(logicQuizes, 6),
        _.sampleSize(mathQuizes, 13),
        _.sampleSize(designQuizes, 8),
        _.sampleSize(iqQuizes, 3)
      );

      setQuizes(
        _.map(quizItems, (e) => {
          let string = _.map(e.question.json.content, (line) => line.content[0].value).join('\n');
          return {
            id: e?.sys?.id,
            question: string,
            questionPicture: e?.questionPicture?.url,
            answerPicture: e?.answerPicture?.url,
            correctAnswers: e.answer.correctAnswer,
            point: e.point,
            answers: _.sampleSize(e?.answer?.answers, e?.answer?.answers?.length + 2),
            type: e.type,
            score: 0,
            userAnswer: null
          };
        })
      );
    }
  }, [data]);

  const updateAnswer = (id: string, answer: string) => {
    setQuizes((quizes) =>
      _.map(quizes, (quiz) => {
        return quiz.id == id ?
          {
            ...quiz, 
            score: _.isArray(quiz.correctAnswers) ?
              (_.includes(quiz.correctAnswers, answer) ? quiz.point : 0) :
              (quiz.correctAnswers == answer ? quiz.point : 0),
            userAnswer: answer
          } :
          quiz
      })
    )
  }

  const finishExam = () => {
    const examScore = _.reduce(quizes, (total, quiz) => total + quiz.score, 0);
    
    updateRecord({'examresult': quizes, 'examscore': examScore})

    navigation.navigate(NavigationRoutes.ExamResultScreen, {
      score: examScore, totalScore: 30
    })
  }


  return (
    <ExamContext.Provider
      value={{
        quizes,
        setQuizes,
        updateAnswer,
        finishExam
      }}
    >
      {children}
    </ExamContext.Provider>
  );
};
