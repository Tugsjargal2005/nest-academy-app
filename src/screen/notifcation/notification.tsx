import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { FlatList } from 'react-native-gesture-handler';
import { Banner, Box, Button, Spacing, Text } from '../../components';
import { Notififcation } from '../../components/illustration/notification';
import { NavigationRoutes } from '../../navigation/navigation-params';

export const NotificationScreen: React.FC<any> = ({ route }) => {
  const navigation = useNavigation();
  const notification = route?.params.notification;
  return (
    <Box>
      <Box>
        {notification.length === 0 ? (
          <Box flexDirection={'column'} width={'100%'} alignItems={'center'}>
            <Spacing mt={22}>
              <Notififcation />
            </Spacing>
            <Spacing mt={12}>
              <Text type={'headline'} width={255} textAlign="center">
                Танд одоогоор notification байхгүй байна
              </Text>
            </Spacing>
            <Spacing mt={15}>
              <Box>
                <Button
                  onPress={() => navigation.navigate(NavigationRoutes.Home)}
                  type={'primary'}
                  height={'s'}
                  width={187}
                >
                  <Text role={'white'} fontFamily={'Montserrat'} bold>
                    Home -руу буцах
                  </Text>
                </Button>
              </Box>
            </Spacing>
          </Box>
        ) : (
          <Box>
            <Spacing mt={10}>
              <FlatList
                data={notification}
                renderItem={({ item }) => (
                  <Spacing m={4}>
                    <Banner type={item.type} title={item.title}>
                      <Text>{item.data}</Text>
                      {item.banner && (
                        <Button
                          category={'text'}
                          type={'primary'}
                          onPress={() => {}}
                        >
                          <Text
                            type={'footnote'}
                            fontFamily={'Montserrat'}
                            underline
                            bold
                          >
                            Мэдээлэл авах
                          </Text>
                        </Button>
                      )}
                    </Banner>
                  </Spacing>
                )}
                keyExtractor={(item) => item.id}
              />
            </Spacing>
          </Box>
        )}
      </Box>
    </Box>
  );
};
