import React from 'react';
import { FlatList, SafeAreaView } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import {
  Border,
  Box,
  Queue,
  Spacing,
  Text,
  Stack,
  Button,
} from '../../components';
import { NewsCard, Register, TryCourse } from './';
import { BackgroundImage } from '../../components/layout/background-image';
import { useNavigation } from '@react-navigation/native';
import { NavigationRoutes } from '../../navigation/navigation-params';
import { useQuery } from '@apollo/client';
import { REQUEST_NEWS } from '../../graphql/queries';
import _ from 'lodash';

import { Header } from '../../components/layout/header';
const exampleImg = require('../../assets/images/image1.png');
const hop = require('../../assets/images/hop.png');
const leap = require('../../assets/images/leap.png');

const Title: React.FC<any> = ({
  title1,
  title2,
  title1Role = 'black',
  title2Role = 'black',
}) => {
  return (
    <Queue size={1}>
      <Text type={'headline'} role={title1Role} bold width={'auto'}>
        {title1}
      </Text>
      <Text type={'headline'} role={title2Role} bold width={'auto'}>
        {title2}
      </Text>
    </Queue>
  );
};

const Course = ({ source, screenName }: any) => {
  const navigation = useNavigation();

  return (
    <Border radius={8}>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate(NavigationRoutes[screenName]);
        }}
      >
        <BackgroundImage
          source={source}
          height={167}
          width={167}
        ></BackgroundImage>
      </TouchableOpacity>
    </Border>
  );
};

const cardsInfo = [
  {
    id: 'card-0',
    title: 'Demo Day 2020 арга хэмжээ амжилттай болж өнгөрлөө.',
    date: '2021 / 4 / 13',
    source: exampleImg,
  },
  {
    id: 'card-1',
    title: 'Demo Day 2020 арга хэмжээ амжилттай болж өнгөрлөө.',
    date: '2021 / 4 / 13',
    source: exampleImg,
  },
  {
    id: 'card-2',
    title: 'Demo Day 2020 арга хэмжээ амжилттай болж өнгөрлөө.',
    date: '2021 / 4 / 13',
    source: exampleImg,
  },
];

const Notification = [
  {
    id: 1,
    type: 'info',
    title: 'Мэдэгдэл',
    data: 'Шинэ элсэлт дуусахад 23 цаг үлдсэн байна. Та яараарай!',
    banner: 'Мэдээлэл авах',
  },
  {
    id: 2,
    type: 'warning',
    title: 'Мэдэгдэл',
    data: 'Шинэ элсэлт дуусахад 23 цаг үлдсэн байна. Та яараарай!',
  },
];

const cources = [
  { poster: hop, screen: 'HopScreen' },
  { poster: leap, screen: 'LeapScreen' },
];

export const HomeScreen: React.FC<any> = () => {
  const navigation = useNavigation();
  const { data, error, loading } = useQuery(REQUEST_NEWS);
  const newsData = data && data.blogPostCollection.items

  return (
    <SafeAreaView style={{ backgroundColor: 'white', flex: 1 }}>
      <Header Notification={Notification} />
      <ScrollView>
        <Register />
        <Spacing ph={4}>
          <Box height={'auto'}>
            <Spacing mv={4}>
              <Title title1={'ҮНДСЭН'} title2={'ХӨТӨЛБӨР'} />
            </Spacing>
            <FlatList
              data={cources}
              renderItem={({ item }) => (
                <Course source={item.poster} screenName={item.screen} />
              )}
              keyExtractor={(_, index) => index + 'course'}
              ItemSeparatorComponent={() => <Box width={16} />}
              showsHorizontalScrollIndicator={false}
              horizontal
            />
          </Box>
        </Spacing>
        <Spacing pl={4} mv={2}>
          <Box height={'auto'}>
            <Spacing mb={4}>
              <Title
                title1={'NEST'}
                title2={'ACADEMY'}
                title2Role={'accentNest'}
              />
            </Spacing>
            <FlatList
              data={newsData}
              renderItem={({ item }) => (
                <NewsCard
                  source={{ uri: item && item.picture.url }}
                  title={item && item.title}
                  date={item && item.publishDate}
                  screenName={'News'}
                />
              )}
              keyExtractor={(item) => item.sys.id}
              ItemSeparatorComponent={() => <Box width={16} />}
              showsHorizontalScrollIndicator={false}
              horizontal
            />
          </Box>
        </Spacing>
        <Spacing pl={4} mv={2}>
          <Box height={'auto'}>
            <Spacing mb={4}>
              <Title
                title1={'NEST'}
                title2={'FOUNDATION'}
                title2Role={'caution400'}
              />
            </Spacing>
            <FlatList
              data={cardsInfo}
              renderItem={({ item }) => (
                <NewsCard
                  source={item.source}
                  title={item.title}
                  date={item.date}
                />
              )}
              keyExtractor={(item) => item.id}
              ItemSeparatorComponent={() => <Box width={16} />}
              showsHorizontalScrollIndicator={false}
              horizontal
            />
          </Box>
        </Spacing>
        <Spacing pl={4} mv={2}>
          <Box height={'auto'}>
            <Spacing mb={4}>
              <Title
                title1={'NEST'}
                title2={'SOLUTIONS'}
                title2Role={'primary500'}
              />
            </Spacing>
            <FlatList
              data={cardsInfo}
              renderItem={({ item }) => (
                <NewsCard
                  source={item.source}
                  title={item.title}
                  date={item.date}
                />
              )}
              keyExtractor={(item) => item.id}
              ItemSeparatorComponent={() => <Box width={16} />}
              showsHorizontalScrollIndicator={false}
              horizontal
            />
          </Box>
        </Spacing>
        <Spacing ph={4} mv={2} mb={10}>
          <TryCourse />
        </Spacing>
        <Stack size={4} alignItems={'center'}>
          <Button
            width={300}
            onPress={() => navigation.navigate(NavigationRoutes.Test)}
            size="l"
          >
            <Text
              fontFamily={'Montserrat'}
              role={'white'}
              type={'callout'}
              textAlign={'center'}
              bold
            >
              Components
            </Text>
          </Button>
          <Button
            width={300}
            onPress={() =>
              navigation.navigate(NavigationRoutes.SuccessScreen, {
                onClick: () => console.log('ok'),
              })
            }
            size="l"
          >
            <Text
              fontFamily={'Montserrat'}
              role={'white'}
              type={'callout'}
              textAlign={'center'}
              bold
            >
              Modal
            </Text>
          </Button>
          <Button
            width={300}
            onPress={() => navigation.navigate(NavigationRoutes.ExamStack)}
            size="l"
          >
            <Text
              fontFamily={'Montserrat'}
              role={'white'}
              type={'callout'}
              textAlign={'center'}
              bold
            >
              Exam
            </Text>
          </Button>
          <Button
            width={300}
            onPress={() => navigation.navigate(NavigationRoutes.ExamResultScreen, {
              score: 30, totalScore: 30
            })}
            size="s"
          >
            <Text
              fontFamily={'Montserrat'}
              role={'white'}
              type={'callout'}
              textAlign={'center'}
              bold
            >
              ExamResultSuccess
            </Text>
          </Button>
          <Button
            onPress={() => navigation.navigate(NavigationRoutes.ExamResultScreen, {
              score: 13, totalScore: 30
            })}
            width={300}
            size="s"
          >
            <Text
              fontFamily={'Montserrat'}
              role={'white'}
              type={'callout'}
              textAlign={'center'}
              bold
            >
              ExamResultFail
            </Text>
          </Button>
        </Stack>
      </ScrollView>
    </SafeAreaView>
  );
};
