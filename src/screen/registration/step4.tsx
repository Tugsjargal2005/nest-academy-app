import React, { useContext, useState } from 'react';
import { ScrollView } from 'react-native';
import {
  Box,
  Text,
  Spacing,
  ProgressStepsContext,
  InputWithMessage,
  InputMessage,
  Stack,
  GroupCheckBox,
  CheckBoxItem,
  MaleIcon,
  FemaleIcon,
  NeutralIcon
} from '../../components';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import _ from 'lodash'

interface Props {
  check?: Object;
}

export const Step4: React.FC<Props> = ({ check }: any) => {
  const { userInfo, setUserInfo } = useContext(ProgressStepsContext);
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date: any) => {
    date =  (_.slice(date.toISOString(), 0, 10)).toString().split('-').join('/').split(',').join('');
    setUserInfo((userInfo: any) => ({ ...userInfo, date: date }));
    hideDatePicker();
  };

  return (
    <Box flex={1} alignItems="center">
      <ScrollView showsVerticalScrollIndicator={false}>
        <Spacing mt={7} mb={8}>
          <Stack size={2} alignItems={'center'}>
            <Text type={'headline'} bold textAlign={'center'} width={287}>
              Хувийн мэдээлэл
            </Text>
            <Text
              width={343}
              role={'primary400'}
              type={'body'}
              textAlign={'center'}
            >
              Дараах мэдээллийг үнэн зөв бөглөснөөр бид танд тохирсон
              хөтөлбөрийг санал болгож чадах юм.
            </Text>
          </Stack>
        </Spacing>

        <Spacing mh={4}>
          <Spacing mb={9.5}>
            <Box flex={1} justifyContent={'center'} alignItems={'center'}>
              <Stack size={4} width={'100%'}>
                <Text width={'100%'} type={'headline'} bold>
                  Төрсөн огноогоо оруулна уу
            </Text>
              <Box flex={1} alignItems={'center'} justifyContent={'center'} >
                <InputWithMessage
                  type={'default'}
                  width={'100%'}
                  value={userInfo.date}
                  messageText={check.date && check.date.messageText}
                  messageType={check.date && check.date.messageType}
                  placeholder={'YYYY/MM/DD'}
                  keyboardType={'number-pad'}
                  onSubmitEditing={() => console.log('nani')}
                  onFocus={() => showDatePicker()}
                />
                <DateTimePickerModal
                  isVisible={isDatePickerVisible}
                  mode="date"
                  onConfirm={handleConfirm}
                  onCancel={hideDatePicker}
                />
              </Box>
            </Stack>
          </Box>
        </Spacing>

          <Stack width={'100%'} size={4}>
            <Text type={'headline'} bold textAlign={'left'} width={'100%'}>
              Хүйсээ сонгоно уу
          </Text>
            <Box width={'100%'} flex={1} alignItems='center' >
              <GroupCheckBox>
                <CheckBoxItem
                  onUnPress={() =>
                    setUserInfo((userInfo: any) => ({ ...userInfo, gender: '' }))
                  }
                  onPress={() =>
                    setUserInfo((userInfo: any) => ({
                      ...userInfo,
                      gender: 'male',
                    }))
                  }
                  checkbox
                  index={0}
                >
                  <Box width={110} height={123}>
                    <Spacing p={5} pb={6} pt={10}>
                      <Box
                        height="100%"
                        justifyContent="space-between"
                        alignItems="center"
                      >
                        <MaleIcon />
                        <Text>Эрэгтэй</Text>
                      </Box>
                    </Spacing>
                  </Box>
                </CheckBoxItem>
                <CheckBoxItem
                  onUnPress={() =>
                    setUserInfo((userInfo: any) => ({ ...userInfo, gender: '' }))
                  }
                  onPress={() =>
                    setUserInfo((userInfo: any) => ({
                      ...userInfo,
                      gender: 'female',
                    }))
                  }
                  checkbox
                  index={1}
                >
                  <Box width={110} height={123}>
                    <Spacing p={5} pb={6} pt={10}>
                      <Box
                        height="100%"
                        justifyContent="space-between"
                        alignItems="center"
                      >
                        <FemaleIcon />
                        <Text>Эмэгтэй</Text>
                      </Box>
                    </Spacing>
                  </Box>
                </CheckBoxItem>
                <CheckBoxItem
                  onUnPress={() =>
                    setUserInfo((userInfo: any) => ({ ...userInfo, gender: '' }))
                  }
                  onPress={() =>
                    setUserInfo((userInfo: any) => ({
                      ...userInfo,
                      gender: 'neutral',
                    }))
                  }
                  checkbox
                  index={2}
                >
                  <Box width={110} height={123}>
                    <Spacing p={5} pb={6} pt={10}>
                      <Box
                        height="100%"
                        justifyContent="space-between"
                        alignItems="center"
                      >
                        <NeutralIcon />
                        <Text>Бусад</Text>
                      </Box>
                    </Spacing>
                  </Box>
                </CheckBoxItem>
              </GroupCheckBox>
            </Box>
            <InputMessage role={check.gender && check.gender.messageType}>
              <Text role={'primary500'} type={'subheading'}>
                {check.gender && check.gender.messageText}
              </Text>
            </InputMessage>
          </Stack>
        </Spacing>
      </ScrollView>
    </Box>

  );
};
