import { useNavigation } from '@react-navigation/core';
import React from 'react';
import { Image, View } from 'react-native';
import {
  ArrowIcon,
  Border,
  Box,
  Button,
  Queue,
  Spacing,
  Stack,
  Text,
} from '../../components';
import { NavigationRoutes } from '../../navigation/navigation-params';
const hop = require('../../assets/images/hop.png');
const leap = require('../../assets/images/leap.png');

interface Props {
  course: 'leap' | 'hop';
}

const data = {
  leap: {
    img: leap,
    title: 'LEAP хөтөлбөр',
    description:
      '18-с дээш насныханд зориулсан сурагч инжинер бэлтгэх хөтөлбөр.',
    to: 'LeapScreen',
  },
  hop: {
    img: hop,
    title: 'HOP хөтөлбөр',
    description: '14-17 насныханд зориулсан сурагч инжинер бэлтгэх хөтөлбөр.',
    to: 'HopScreen',
  },
};

export const CourseCard: React.FC<Props> = ({ course }) => {
  const navigation = useNavigation();

  return (
    <Border radius={8}>
      <Box width={'100%'} height={178} role={'fawhite'}>
        <Spacing p={4}>
          <Stack size={4}>
            <Queue>
              <Stack size={1}>
                <Text type={'headline'} bold width={204}>
                  {data[course].title}
                </Text>
                <Text type={'footnote'} width={204}>
                  {data[course].description}
                </Text>
              </Stack>
              <Image
                source={data[course].img}
                style={{ width: 88, height: 88, borderRadius: 8 }}
              />
            </Queue>
            <Button
              onPress={() =>
                navigation.navigate(NavigationRoutes[data[course].to])
              }
              category={'fill'}
              size={'l'}
              width={307}
            >
              <Box flexDirection={'row'} alignItems={'center'}>
                <Text
                  type={'subheading'}
                  fontFamily={'Montserrat'}
                  bold
                  role={'white'}
                  width={'auto'}
                >
                  Дэлгэрэнгүй
                </Text>
                <Spacing ml={1}>
                  <View style={{ transform: [{ rotate: '-90deg' }] }}>
                    <ArrowIcon color={'white'} height={6} width={12} />
                  </View>
                </Spacing>
              </Box>
            </Button>
          </Stack>
        </Spacing>
      </Box>
    </Border>
  );
};
