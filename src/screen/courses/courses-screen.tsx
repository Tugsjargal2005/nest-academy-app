import React, { useContext } from 'react';
import { FlatList, ScrollView, TouchableOpacity, SafeAreaView } from 'react-native';
import { useQuery } from '@apollo/client';
import { useNavigation } from '@react-navigation/core';

import { AuthContext } from '../../provider/auth';
import { REQUEST_MICRO_COURSES, REQUEST_LESSONS, REQUEST_MAIN_COURSES } from '../../graphql';
import { NavigationRoutes } from '../../navigation/navigation-params';
import { Border, Box, Spacing, Text, BackgroundImage, Button, RightArrowIcon, ScrollCard, LessonDashboard, LastSeenVideo, Header, Stack, Queue } from '../../components';
import _ from 'lodash';

const MicroCourseCard = ({ navigation, source, route }: any) => {
  return (
    <Border radius={8}>
      <TouchableOpacity onPress={() => navigation.navigate(route)}>
        <BackgroundImage source={source} height={170} width={170} resizeMode={'contain'} />
      </TouchableOpacity>
    </Border>
  )
}

const ButtonOnThisScreen = ({ text, onPress }: any) => {
  return (
    <Button width={'100%'} onPress={() => onPress()}>
      <Queue size={3} alignItems={'center'}>
        <Text bold role={'white'} type={'callout'} fontFamily="Montserrat">
          {text}
        </Text>
        <RightArrowIcon />
      </Queue>
    </Button>
  )
}

const SubTitle = ({ text }: any) => {
  return (
    <Spacing mt={4}>
      <Text type="headline" role="black" bold> {text} </Text>
    </Spacing>
  )
}

export const CourseScreen = () => {
  const navigation = useNavigation();
  const { user } = useContext(AuthContext);
  const coursesQuery = useQuery(REQUEST_MICRO_COURSES);
  const lessonsQuery = useQuery(REQUEST_LESSONS);
  const mainCoursesQuery = useQuery(REQUEST_MAIN_COURSES);

  const mainCourses = mainCoursesQuery.data && mainCoursesQuery.data.mainCourseCollection.items;
  const courses = coursesQuery.data && coursesQuery.data.microCourseCollection.items;
  const lessons = lessonsQuery.data && lessonsQuery.data.lessonCollection.items;

  return (
    <SafeAreaView>
      <Header Notification={[]} />
      <ScrollView style={{ paddingHorizontal: 16 }} showsVerticalScrollIndicator={false}>
        <Stack size={4}>

          {
            user
              ?
              <LastSeenVideo onPress={() => navigation.navigate(NavigationRoutes.FreeCourseScreen, { course: courses[0], lessonURL: 'https://videos.ctfassets.net/1d2jz7tg7egg/7wLWb1uDExtLjfZDvFxexu/81a3f7377788d85a3b43649f5812f724/Nested_Sequence_04.mp4' })} />
              :
              <Border radius={8}>
                <Box role="white" width={'100%'} height={173}>
                  <Spacing p={4}>
                    <Stack size={3}>
                      <Text type="title3" fontFamily="Montserrat" role="primary500" bold={true}>
                        Илүү ихийг үз
                      </Text>
                      <Text type="subheading" role="primary400" width={312} numberOfLines={2}>
                        Та манай аппликэшинд бүртгүүлснээр цааш илүү их хичээлүүдийг үзэх боломжтой болно.
                      </Text>
                      <ButtonOnThisScreen onPress={() => navigation.navigate(NavigationRoutes.MainCourseRegistration)} text={"БҮРТГҮҮЛЭХ"} />
                    </Stack>
                  </Spacing>
                </Box>
              </Border>
          }

          <SubTitle text={'ТУРШИЛТЫН ХИЧЭЭЛ'} />
          <FlatList
            data={courses}
            renderItem={({ item }) => (
              <ScrollCard
                onPress={() => navigation.navigate(NavigationRoutes.FreeCourseScreen, { course: item, lessonId: null })}
                source={item.posterImage.url}
                title={item.title}
                description={item.description.json.content[0].content[0].value}
              />
            )}
            ItemSeparatorComponent={() => <Box width={16} />}
            keyExtractor={(item) => item.sys.id}
            showsHorizontalScrollIndicator={false}
            horizontal
          />

          <SubTitle text={'ҮНДСЭН ХӨТӨЛБӨР'} />
          <FlatList
            data={mainCourses}
            renderItem={({ item }) => (
              <MicroCourseCard navigation={navigation} route={item.courseTitle + 'Screen'} source={{ uri: item.courseLogo.url }} title={item.courseTitle} />
            )}
            ItemSeparatorComponent={() => <Box width={16} />}
            keyExtractor={(item) => item.sys.id}
            horizontal
          />

          <SubTitle text={'ХАМГИЙН ИХ ҮЗЭЛТТЭЙ'} />
          <Stack size={4}>
            {lessons && lessons.map((item: any) => (
              <LessonDashboard
                onPress={() => navigation.navigate(NavigationRoutes.FreeCourseScreen, { course: courses[_.findIndex(courses, { title: item.linkedFrom.microCourseCollection.items[0].title })], lessonURL: item.video.url })}
                source={{ uri: item.posterImage.url }}
                desc={item.article}
                name={item.name}
                type={item.isLock ? 'locked' : 'unlocked'}
                key={item.sys.id}
                circleButton
              />
            ))}
          </Stack>
          <ButtonOnThisScreen onPress={() => console.log('hey')} text={"Цааш үзэх"} />

          <Box height={120} />
        </Stack>
      </ScrollView>
    </SafeAreaView>
  );
};
