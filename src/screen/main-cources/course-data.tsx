import { DesignIcon } from '../../components/icons';
export const VoluData = {
  name: 'VOLU',
  source: require('../../assets/images/volu.png'),
  source2: require('../../assets/images/volu2.png'),
  introduction:
    'Дэлхийн хамгийн том сошиал платформыг ажиллуулдаг Фэйсбүүк компани Twitter, Clubhouse зэрэг бүтээгдэхүүнүүдтэй өрсөлдөх зорилгоор аудио үйлчилгээнүүд нэвтрүүлэх гэж байгаагаа өчигдөр зарлажээ.\n\nӨчигдөр хэвлэлд ярилцлага өгөх үеэрээ Фэйсбүүкийн гүйцэтгэх захирал Марк Цукербэрг энэ тухай анх мэдээлсэн байна. Энэ оны зунаас зах зээлд нэвтрэх шинэ үйлчилгээнүүд нь богино аудио клип илгээх “Soundbites”, аудио хэлэлцүүлэг өрнүүлдэг. Спортын амжилт гаргаж буй Монгол Улсын Үндэсний',
  students: [
    {
      id: 1,
      name: 'М.Ганбат',
      title: 'Дизайн',
      icon: DesignIcon,
      source: require('../../assets/images/ganbat.png'),
    },
    {
      id: 22,
      name: 'Э.Дөлгөөн',
      title: 'Дизайн',
      icon: DesignIcon,
      source: require('../../assets/images/ganbat.png'),
    },
  ],
  comments: [
    {
      id: 1,
      name: 'Н. Шагай',
      title: 'Founder',
      description:
        'Нэст академид суралцаж эхлэхээс өмнө надад мэргэжлээ сонгоход хүндрэлтэй байсан. Харин энд суралцаж эхэлснээр өөрийхөө юунд дуртайг мэдэж, график дизайнер болохоор шийдсэн.',
      source: require('../../assets/images/shagai.png'),
    },
    {
      id: 2,
      name: 'Н. Цэрэнлхагва',
      title: '3-р түвшний сурагч',
      description:
        'Нэст академид суралцаж эхлэхээс өмнө надад мэргэжлээ сонгоход хүндрэлтэй байсан. Харин энд суралцаж эхэлснээр өөрийхөө юунд дуртайг мэдэж, график дизайнер болохоор шийдсэн.',
      source: require('../../assets/images/zedude.png'),
    },
  ],
};
