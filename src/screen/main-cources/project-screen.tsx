import React from 'react';
import { FlatList, SafeAreaView, ScrollView } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { NavigationRoutes } from '../../navigation/navigation-params';
import { VoluData } from './course-data';
import {
  BackgroundImage,
  Box,
  Queue,
  Spacing,
  Stack,
  Text,
  Border,
  Button,
  Comment,
} from '../../components';
import { TouchableOpacity } from 'react-native-gesture-handler';

const setgel = require('../../assets/images/setgel.png');
const volu = require('../../assets/images/volu.png');
const dul = require('../../assets/images/dul.png');

const CommentData = [
  {
    id: 1,
    source: require('../../assets/images/zedude.png'),
    name: 'Н. Цэрэнлхагва',
    title: '3-р түвшний сурагч',
    description:
      'Нэст академид суралцаж эхлэхээс өмнө надад мэргэжлээ сонгоход хүндрэлтэй байсан. Харин энд суралцаж эхэлснээр өөрийхөө юунд дуртайг мэдэж, график дизайнер болохоор шийдсэн.',
  },
  {
    id: 2,
    source: require('../../assets/images/zedude.png'),
    name: 'Н. Цэрэнлхагва',
    title: '3-р түвшний сурагч',
    description:
      'Нэст академид суралцаж эхлэхээс өмнө надад мэргэжлээ сонгоход хүндрэлтэй байсан. Харин энд суралцаж эхэлснээр өөрийхөө юунд дуртайг мэдэж, график дизайнер болохоор шийдсэн.',
  },
];

export const ProjectScreen: React.FC<any> = ({ ...props }) => {
  const navigation = useNavigation();
  return (
    <SafeAreaView>
      <Box width="100%" height="150%">
        <ScrollView>
          <Box>
            <Spacing mb={3} mt={5} ml={3}>
              <Text type="headline" role="primary500" bold textAlign="left">
                Demo Day 2020
              </Text>
            </Spacing>
            <Box width="100%">
              <Stack size={2}>
                <Queue justifyContent="center">
                  <Spacing mr={2}>
                    <TouchableOpacity
                      onPress={() =>
                        navigation.navigate(
                          NavigationRoutes.DemoDetails,
                          VoluData
                        )
                      }
                    >
                      <BackgroundImage source={setgel} width={190} />
                    </TouchableOpacity>
                  </Spacing>
                  <TouchableOpacity
                    onPress={() =>
                      navigation.navigate(
                        NavigationRoutes.DemoDetails,
                        VoluData
                      )
                    }
                  >
                    <BackgroundImage source={volu} width={190} />
                  </TouchableOpacity>
                </Queue>
                <Queue justifyContent="center">
                  <Spacing mr={2}>
                    <TouchableOpacity
                      onPress={() =>
                        navigation.navigate(
                          NavigationRoutes.DemoDetails,
                          VoluData
                        )
                      }
                    >
                      <BackgroundImage source={dul} width={190} />
                    </TouchableOpacity>
                  </Spacing>
                  <TouchableOpacity
                    onPress={() =>
                      navigation.navigate(
                        NavigationRoutes.DemoDetails,
                        VoluData
                      )
                    }
                  >
                    <BackgroundImage source={setgel} width={190} />
                  </TouchableOpacity>
                </Queue>
              </Stack>
            </Box>
            <Spacing mv={5} mh={3}>
              <Border lineWidth={1} role="primary500" radius={4}>
                <Box width="100%" height={48} justifyContent="center">
                  <Text textAlign="center" bold type="callout">
                    Өөр
                  </Text>
                </Box>
              </Border>
            </Spacing>
          </Box>
          <Box>
            <Spacing mv={5} ml={3}>
              <Text type="title3" role="primary500" bold>
                Сурагцын сэтгэгдэл
              </Text>
            </Spacing>
            <FlatList
              data={CommentData}
              horizontal
              renderItem={({ item }) => (
                <Comment
                  name={item.name}
                  source={item.source}
                  title={item.title}
                  description={item.description}
                  width={311}
                  height={288}
                />
              )}
            ></FlatList>
          </Box>
        </ScrollView>
      </Box>
    </SafeAreaView>
  );
};
