import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Image,
} from 'react-native';
import { Box, Queue, Spacing, Stack } from '../../components/layout';
import { Text } from '../../components/core/text';
import { Button } from '../../components/core/button';
import { Border } from '../../components/core';
import { NoInternetConnectionIllustration } from '../../components/illustration';
import { NavigationRoutes } from '../../navigation/navigation-params';

export const NoInternetConnectionScreen: React.FC<ModalType> = ({
  navigation,
  route,
}) => {
  const { title, description } = route.params || {};
  const content =
    'Интернет холболт салсан байна. Та холболтоо шалгаад апп аа дахин нээнэ үү.';

  return (
    <Box flex={1} justifyContent={'center'} alignItems={'center'}>
      <TouchableOpacity
        style={StyleSheet.absoluteFill}
        onPress={() => navigation.goBack()}
      >
        <View
          style={[
            StyleSheet.absoluteFill,
            { backgroundColor: 'rgba(18,18,18,0.4)' },
          ]}
        ></View>
      </TouchableOpacity>
      <Border radius={8}>
        <Box width={Dimensions.get('window').width * 0.9} role={'primary100'}>
          <Spacing mt={15} mb={15} mr={10} ml={10}>
            <Stack size={6} justifyContent={'center'} alignItems={'center'}>
              <Image source={require('../../assets/images/wifi.png')} />
              <Text
                type={'title1'}
                bold
                fontFamily={'Montserrat'}
                textAlign={'center'}
              >
                {title || 'Ooppss !!'}
              </Text>
              <Text
                fontFamily={'Montserrat'}
                type={'body'}
                textAlign={'center'}
              >
                {description || content}
              </Text>
              <Queue justifyContent={'center'}>
                <Button
                  width={200}
                  onPress={() => navigation.navigate(NavigationRoutes.MainRoot)}
                  status="default"
                >
                  <Text
                    fontFamily={'Montserrat'}
                    role={'white'}
                    type={'callout'}
                    textAlign={'center'}
                    bold
                  >
                    Үргэлжлүүлэх
                  </Text>
                </Button>
              </Queue>
            </Stack>
          </Spacing>
        </Box>
      </Border>
    </Box>
  );
};

type ModalType = {
  route: any;
  navigation: any;
};
