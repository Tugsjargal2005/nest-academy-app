import React from 'react';
import { View, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import { Box, Queue, Spacing, Stack } from '../../components/layout';
import { Text } from '../../components/core/text';
import { Button } from '../../components/core/button';
import { Border } from '../../components/core';
import { SomethingWentWrongIllustration } from '../../components/illustration';
import { NavigationRoutes } from '../../navigation/navigation-params';

export const SomethingWentWrongScreen: React.FC<ModalType> = ({
  navigation,
  route,
}) => {
  const { title, description } = route.params || {};
  const content =
    'Уучлаарай апп дээр алдаа гарлаа та сүүлд хийсэн ажлаа шалгаад дахин оролдно уу.';

  return (
    <Box flex={1} justifyContent={'center'} alignItems={'center'}>
      <TouchableOpacity
        style={StyleSheet.absoluteFill}
        onPress={() => navigation.goBack()}
      >
        <View
          style={[
            StyleSheet.absoluteFill,
            { backgroundColor: 'rgba(18,18,18,0.4)' },
          ]}
        ></View>
      </TouchableOpacity>
      <Border radius={8}>
        <Box width={Dimensions.get('window').width * 0.9} role={'primary100'}>
          <Spacing mt={15} mb={15} mr={10} ml={10}>
            <Stack size={6} justifyContent={'center'} alignItems={'center'}>
              <SomethingWentWrongIllustration />
              <Spacing mb={5} />
              <Text
                type={'title1'}
                bold
                fontFamily={'Montserrat'}
                textAlign={'center'}
              >
                {title || 'Ooppss !!'}
              </Text>
              <Text
                fontFamily={'Montserrat'}
                type={'body'}
                textAlign={'center'}
              >
                {description || content}
              </Text>
              <Queue justifyContent={'center'}>
                <Button
                  width={200}
                  onPress={() => navigation.navigate(NavigationRoutes.MainRoot)}
                  status="default"
                >
                  <Text
                    fontFamily={'Montserrat'}
                    role={'white'}
                    type={'callout'}
                    textAlign={'center'}
                    bold
                  >
                    Үргэлжлүүлэх
                  </Text>
                </Button>
              </Queue>
            </Stack>
          </Spacing>
        </Box>
      </Border>
    </Box>
  );
};
const styles = StyleSheet.create({
  img: {
    width: 168,
    height: 158,
  },
});

type ModalType = {
  route: any;
  navigation: any;
};
