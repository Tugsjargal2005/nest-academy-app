import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Route,
  TouchableOpacity,
} from 'react-native';
import { Box, Queue, Spacing, Stack } from '../../components/layout';
import { Text } from '../../components/core/text';
import { InternetFailed } from '../../components/icons';
import { Button } from '../../components/core/button';
import { Border } from '../../components/core';

export const Modal: React.FC<ModalType> = ({ navigation, route }) => {
  const { title, description, icon, onClick } = route.params || {};
  const content =
    'Интернет холболт салсан байна. Та холболтоо шалгаад апп аа дахин нээнэ үү.';

  return (
    <Box flex={1} justifyContent={'center'} alignItems={'center'}>
      <TouchableOpacity
        style={StyleSheet.absoluteFill}
        onPress={() => navigation.goBack()}
      >
        <View
          style={[
            StyleSheet.absoluteFill,
            { backgroundColor: 'rgba(18,18,18,0.4)' },
          ]}
        ></View>
      </TouchableOpacity>
      <Border radius={8}>
        <Box width={Dimensions.get('window').width * 0.9} role={'primary100'}>
          <Spacing mt={15} mb={15} mr={10} ml={10}>
            <Stack size={6}>
              <Queue justifyContent={'center'}>
                {icon || (
                  <Border radius={1000}>
                    <Box width={225} height={225} role={'primary500'}>
                      <InternetFailed />
                    </Box>
                  </Border>
                )}
              </Queue>
              <Text
                type={'title1'}
                bold
                fontFamily={'Montserrat'}
                textAlign={'center'}
              >
                {title || 'Ooppss !!'}
              </Text>
              <Text
                fontFamily={'Montserrat'}
                type={'body'}
                textAlign={'center'}
              >
                {description || content}
              </Text>
              <Queue justifyContent={'center'}>
                <Button width={200} onPress={onClick} status="default">
                  <Text
                    width={150}
                    fontFamily={'Montserrat'}
                    role={'white'}
                    type={'callout'}
                    textAlign={'center'}
                    bold
                  >
                    Үргэлжлүүлэх
                  </Text>
                </Button>
              </Queue>
            </Stack>
          </Spacing>
        </Box>
      </Border>
    </Box>
  );
};

type ModalType = {
  route: any;
  navigation: any;
};
