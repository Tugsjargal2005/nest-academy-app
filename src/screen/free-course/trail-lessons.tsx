import React from 'react';
import { ScrollView } from 'react-native';
import { Text } from '../../components/core';
import { Spacing, Stack } from '../../components/layout';
import { LessonDashboard } from '../../components';

export const FCStudyScreen = (prams: any) => {
  const { lessonsCollection, setLesosnUrl } = prams.route.params;

  return (
    <ScrollView  style={{backgroundColor: '#FAFAFA'}}>
      <Spacing pt={9} pb={6} pl={4} pr={4}>
        <Stack size={4}>
          <Text type={'headline'} role={'primary500'} bold={true}>
            Эхлэл хичээл
          </Text>

          {lessonsCollection.items.map((item: any) => (
            <LessonDashboard
              onPress={() => setLesosnUrl(item.video.url)}
              source={{ uri: item.posterImage.url }}
              desc={item.article}
              name={item.name}
              type={item.isLock ? 'locked' : 'unlocked'}
              key={item.sys.id}
              circleButton
            />
          ))}

        </Stack>
      </Spacing>
    </ScrollView>
  );
};