import React, {useContext} from 'react';

import {
  Box,
  Stack,
  Text,
  Spacing,
  Border,
  Button,
  LozengeStatus,
  AdmissionProcessCard,
  AdmissionProcessCardHeader,
  AdmissionProcessCardContent,
  CheckIconTwo,
} from '../../components';
import { useNavigation } from '@react-navigation/native';
import { NavigationRoutes } from '../../navigation/navigation-params'
import { useDocument} from '../../hooks';
import { AuthContext } from '../../provider/auth';




type StepType = {
  stepNumber: number;
  title: string;
  children: any;
  statusP?: 'success' | 'error' | 'pending' | 'default' | 'none' | 'active';
  buttonType: 'signup' | 'test' | 'interview' | 'contract';
  activeStep: number;
};

const StartButton: React.FC<any> = ({ authstatus, stepNumber }) => {
  const navigation = useNavigation();
  return (

    <Spacing ml={4} mb={3} mr={4} mt={4}>
      <Button status={authstatus === 'pending' ? 'disabled' : 'default'} width={'100%'} onPress={() => { stepNumber === 3 && navigation.navigate(NavigationRoutes.BookMeeting)|| stepNumber === 2 && navigation.navigate(NavigationRoutes.ExamStack) || stepNumber === 1 && navigation.navigate(NavigationRoutes.SignUp) }}>
        <Text
          fontFamily={'Montserrat'}
          role={'white'}
          type={'callout'}
          textAlign={'center'}
          bold
        >
          Эхлэх
        </Text>
      </Button>
    </Spacing>
  )
}



const AdmissionProcess: React.FC<any> = ({ children, programId }) => {
  return (
    <Stack size={3}>
      {React.Children.toArray(children).map((child, index) => {
        return React.cloneElement(child, { stepNumber: index + 1, programId });
      })}
    </Stack>
  )
}


const Step: React.FC<any> = ({authstep, authstatus, stepNumber, role, children, title, statusP, buttonStatus, programId }) => {
  return (

    <Box width={'100%'} height={'auto'} flexDirection={'row'}>
      <Box flex={1} alignItems={'flex-start'}>
        <Box flex={1} alignItems={'center'}>
          <CircleStep stepNumber={stepNumber} authstep={authstep} />
          <VerticalLine stepNumber={stepNumber} authstep={authstep} />
        </Box>
      </Box>
      <Box flex={5}>
        <AdmissionProcessCard>
          <AdmissionProcessCardHeader>
            <Spacing ph={3} pv={3} >
            {stepNumber < authstep &&
              <LozengeStatus
                type={'success'}
                style={'subtle'}
                status={'АМЖИЛТТАЙ'}
              
              />
              ||
              stepNumber === authstep && authstatus === 'error' &&
              <LozengeStatus
                type={'error'}
                style={'subtle'}
                status={'АМЖИЛТГҮЙ'}

              />
              ||
              stepNumber === authstep && authstatus === 'pending' &&
              <LozengeStatus
                type={'pending'}
                style={'subtle'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}

              />
              ||
              stepNumber === authstep && authstatus === 'default' &&
              <LozengeStatus
                type={'default'}
                style={'subtle'}
                status={'DEFAULT'}

              />}
            <Text bold type={'headline'} fontFamily={'Montserrat'} role={'black'}>{title}</Text>
            </Spacing>
          </AdmissionProcessCardHeader>
          <AdmissionProcessCardContent>
            <Spacing p={3}>
              {children}
            </Spacing>
          </AdmissionProcessCardContent>
          {
            stepNumber === authstep && <StartButton authstatus={authstatus}  stepNumber={stepNumber} />
          }
        </AdmissionProcessCard>
      </Box>
    </Box>
  )
}

const CircleStep: React.FC<any> = ({ stepNumber, authstep, statusP }) => {
  return (
    <Box height={32} width={32}>
      <Border radius={16} lineWidth={1} role={stepNumber <= authstep && 'success500' || 'black'}>
        <Box height={'100%'} width={'100%'} role={stepNumber <= authstep && 'success200' || 'white'} alignItems={'center'} justifyContent={'center'}>
          {statusP === 'success' ? <CheckIconTwo height={20} width={20}></CheckIconTwo>
            : <Text>{stepNumber}</Text>}
        </Box>
      </Border>
    </Box>
  )
}

const VerticalLine: React.FC<any> = ({ authstep, stepNumber }) => {
  return (
    <Box flex={1} zIndex={2} width={0} opacity={1}>
      <Spacing mt={2}>
        <Border type={'dashed'} lineWidth={1} lineHeight={30} role={stepNumber < authstep ? 'success300' : 'gray'} >
          <Box height={'100%'} width={0}></Box>
        </Border>
      </Spacing>
    </Box>
  )
}

export const AdmissionScreen = () => {
  const navigation = useNavigation();
  const { user, signOut } = useContext(AuthContext);
  const { doc } = useDocument(`users/${user?.uid}`);
  const { authstep} : any = doc || {};
  const { authstatus} : any = doc || {};
  const { programId} : any = doc || {};
  console.log(authstep);


  return (
    <Box flex={1} role={'fawhite'}>
      <Spacing ml={4} mt={4} mr={4}>
        <AdmissionProcess programId={programId}>
          <Step title={'Бүртгэл'} authstep={authstep} authstatus={authstatus}>
            <Spacing mb={2} >
              <Text type={'footnote'} role={'primary500'}>
                Та HOP хөтөлбөрт амжилттай бүртгүүллээ. Та шалгалтаа өгснөөр элсэлтэнд бүрэн хамрагдана.
              </Text>
            </Spacing>
          </Step>
          <Step title={'Шалгалт'} authstep={authstep} authstatus={authstatus}>
            <Stack size={4}>
              <Text type={'footnote'} role={'primary500'}>
                Шалгалтын асуултанд логик сэтгэлгээ болон гоо зүйн мэдрэмжийг шалгасан 30 асуулт байна. Танд амжилт хүсье!
              </Text>
            </Stack>
          </Step>
          <Step title={'Ярилцлага'} authstep={authstep} authstatus={authstatus}>
            <Spacing mb={2} >
              <Text type={'footnote'} role={'primary500'}>Та шалгалтанд тэнцсэн тохиолдолд мастерууд тантай ярилцлага хийх болно.</Text>
            </Spacing>
          </Step>
          <Step title={'Гэрээ'} authstep={authstep} authstatus={authstatus}>
            <Spacing mb={2} >
              <Text type={'footnote'} role={'primary500'}>Та бидэнтэй сургалтын гэрээ байгуулснаар бид таны суудлыг баталгаажуулах болно! </Text>
            </Spacing>
          </Step>
        </AdmissionProcess>
      </Spacing>
    </Box>

  )
};